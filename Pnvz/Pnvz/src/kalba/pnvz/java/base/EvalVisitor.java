package kalba.pnvz.java.base;

import kalba.pnvz.java.generated.PnvzBaseVisitor;
import kalba.pnvz.java.generated.PnvzParser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.TerminalNode;
import kalba.pnvz.java.generated.PnvzParser.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EvalVisitor extends PnvzBaseVisitor<PnvzValue> {
	private static ReturnValue returnValue = new ReturnValue();
    private Scope scope;
    private Map<String, Function> functions;
    
    public EvalVisitor(Scope scope, Map<String, Function> functions) {
        this.scope = scope;
        this.functions = functions;
    }

    // functionDecl
    @Override
    public PnvzValue visitFunctionDecl(FunctionDeclContext ctx) {
        return PnvzValue.VOID;
    }
    

    
    




    // expression '*' expression                #multiplyExpression
    @Override
    public PnvzValue visitMultiplyExpression(MultiplyExpressionContext ctx) {
    	PnvzValue lhs = this.visit(ctx.expression(0));
    	PnvzValue rhs = this.visit(ctx.expression(1));
    	if(lhs == null || rhs == null) {
    		System.err.println("lhs "+ lhs+ " rhs "+rhs);
    	    throw new EvalException(ctx);
    	}
    	
    	// number * number
        if(lhs.isNumber() && rhs.isNumber()) {
            return new PnvzValue(lhs.asDouble() * rhs.asDouble());
        }

        // string * number
        if(lhs.isString() && rhs.isNumber()) {
            StringBuilder str = new StringBuilder();
            int stop = rhs.asDouble().intValue();
            for(int i = 0; i < stop; i++) {
                str.append(lhs.asString());
            }
            return new PnvzValue(str.toString());
        }

        // list * number
        if(lhs.isList() && rhs.isNumber()) {
            List<PnvzValue> total = new ArrayList<PnvzValue>();
            int stop = rhs.asDouble().intValue();
            for(int i = 0; i < stop; i++) {
                total.addAll(lhs.asList());
            }
            return new PnvzValue(total);
        }    	
    	throw new EvalException(ctx);
    }

    // expression '/' expression                #divideExpression
    @Override
    public PnvzValue visitDivideExpression(DivideExpressionContext ctx) {
    	PnvzValue lhs = this.visit(ctx.expression(0));
    	PnvzValue rhs = this.visit(ctx.expression(1));
    	if (lhs.isNumber() && rhs.isNumber()) {
    		return new PnvzValue(lhs.asDouble() / rhs.asDouble());
    	}
    	throw new EvalException(ctx);
    }

    // expression '%' expression                #modulusExpression
	@Override
	public PnvzValue visitModulusExpression(ModulusExpressionContext ctx) {
		PnvzValue lhs = this.visit(ctx.expression(0));
    	PnvzValue rhs = this.visit(ctx.expression(1));
    	if (lhs.isNumber() && rhs.isNumber()) {
    		return new PnvzValue(lhs.asDouble() % rhs.asDouble());
    	}
    	throw new EvalException(ctx);
	}
	
    // expression '+' expression                #addExpression
    @Override
    public PnvzValue visitAddExpression(@NotNull PnvzParser.AddExpressionContext ctx) {
        PnvzValue lhs = this.visit(ctx.expression(0));
        PnvzValue rhs = this.visit(ctx.expression(1));
        
        if(lhs == null || rhs == null) {
            throw new EvalException(ctx);
        }
        
        // number + number
        if(lhs.isNumber() && rhs.isNumber()) {
            return new PnvzValue(lhs.asDouble() + rhs.asDouble());
        }
        
        // list + any
        if(lhs.isList()) {
            List<PnvzValue> list = lhs.asList();
            list.add(rhs);
            return new PnvzValue(list);
        }

        // string + any
        if(lhs.isString()) {
            return new PnvzValue(lhs.asString() + "" + rhs.toString());
        }

        // any + string
        if(rhs.isString()) {
            return new PnvzValue(lhs.toString() + "" + rhs.asString());
        }
        
        return new PnvzValue(lhs.toString() + rhs.toString());
    }

    // expression '-' expression                #subtractExpression
    @Override
    public PnvzValue visitSubtractExpression(SubtractExpressionContext ctx) {
    	PnvzValue lhs = this.visit(ctx.expression(0));
    	PnvzValue rhs = this.visit(ctx.expression(1));
    	if (lhs.isNumber() && rhs.isNumber()) {
    		return new PnvzValue(lhs.asDouble() - rhs.asDouble());
    	}
    	if (lhs.isList()) {
            List<PnvzValue> list = lhs.asList();
            list.remove(rhs);
            return new PnvzValue(list);
        }
    	throw new EvalException(ctx);
    }

    // expression '>=' expression               #gtEqExpression
    @Override
    public PnvzValue visitGtEqExpression(GtEqExpressionContext ctx) {
    	PnvzValue lhs = this.visit(ctx.expression(0));
    	PnvzValue rhs = this.visit(ctx.expression(1));
    	if (lhs.isNumber() && rhs.isNumber()) {
    		return new PnvzValue(lhs.asDouble() >= rhs.asDouble());
    	}
    	if(lhs.isString() && rhs.isString()) {
            return new PnvzValue(lhs.asString().compareTo(rhs.asString()) >= 0);
        }
    	throw new EvalException(ctx);
    }

    // expression '<=' expression               #ltEqExpression
    @Override
    public PnvzValue visitLtEqExpression(LtEqExpressionContext ctx) {
    	PnvzValue lhs = this.visit(ctx.expression(0));
    	PnvzValue rhs = this.visit(ctx.expression(1));
    	if (lhs.isNumber() && rhs.isNumber()) {
    		return new PnvzValue(lhs.asDouble() <= rhs.asDouble());
    	}
    	if(lhs.isString() && rhs.isString()) {
            return new PnvzValue(lhs.asString().compareTo(rhs.asString()) <= 0);
        }
    	throw new EvalException(ctx);
    }

    // expression '>' expression                #gtExpression
    @Override
    public PnvzValue visitGtExpression(GtExpressionContext ctx) {
    	PnvzValue lhs = this.visit(ctx.expression(0));
    	PnvzValue rhs = this.visit(ctx.expression(1));
    	if (lhs.isNumber() && rhs.isNumber()) {
    		return new PnvzValue(lhs.asDouble() > rhs.asDouble());
    	}
    	if(lhs.isString() && rhs.isString()) {
            return new PnvzValue(lhs.asString().compareTo(rhs.asString()) > 0);
        }
    	throw new EvalException(ctx);
    }

    // expression '<' expression                #ltExpression
    @Override
    public PnvzValue visitLtExpression(LtExpressionContext ctx) {
    	PnvzValue lhs = this.visit(ctx.expression(0));
    	PnvzValue rhs = this.visit(ctx.expression(1));
    	if (lhs.isNumber() && rhs.isNumber()) {
    		return new PnvzValue(lhs.asDouble() < rhs.asDouble());
    	}
    	if(lhs.isString() && rhs.isString()) {
            return new PnvzValue(lhs.asString().compareTo(rhs.asString()) < 0);
        }
    	throw new EvalException(ctx);
    }

    // expression '==' expression               #eqExpression
    @Override
    public PnvzValue visitEqExpression(@NotNull PnvzParser.EqExpressionContext ctx) {
        PnvzValue lhs = this.visit(ctx.expression(0));
        PnvzValue rhs = this.visit(ctx.expression(1));
        if (lhs == null) {
        	throw new EvalException(ctx);
        }
        return new PnvzValue(lhs.equals(rhs));
    }

    // expression '!=' expression               #notEqExpression
    @Override
    public PnvzValue visitNotEqExpression(@NotNull PnvzParser.NotEqExpressionContext ctx) {
        PnvzValue lhs = this.visit(ctx.expression(0));
        PnvzValue rhs = this.visit(ctx.expression(1));
        return new PnvzValue(!lhs.equals(rhs));
    }

    // expression '&&' expression               #andExpression
    @Override
    public PnvzValue visitAndExpression(AndExpressionContext ctx) {
    	PnvzValue lhs = this.visit(ctx.expression(0));
    	PnvzValue rhs = this.visit(ctx.expression(1));
    	
    	if(!lhs.isBoolean() || !rhs.isBoolean()) {
    	    throw new EvalException(ctx);
        }
		return new PnvzValue(lhs.asBoolean() && rhs.asBoolean());
    }

    // expression '||' expression               #orExpression
    @Override
    public PnvzValue visitOrExpression(OrExpressionContext ctx) {
    	PnvzValue lhs = this.visit(ctx.expression(0));
    	PnvzValue rhs = this.visit(ctx.expression(1));
    	
    	if(!lhs.isBoolean() || !rhs.isBoolean()) {
    	    throw new EvalException(ctx);
        }
		return new PnvzValue(lhs.asBoolean() || rhs.asBoolean());
    }



    // expression In expression                 #inExpression
	@Override
	public PnvzValue visitInExpression(InExpressionContext ctx) {
		PnvzValue lhs = this.visit(ctx.expression(0));
    	PnvzValue rhs = this.visit(ctx.expression(1));
    	
    	if (rhs.isList()) {
    		for(PnvzValue val: rhs.asList()) {
    			if (val.equals(lhs)) {
    				return new PnvzValue(true);
    			}
    		}
    		return new PnvzValue(false);
    	}
    	throw new EvalException(ctx);
	}
	
    // Number                                   #numberExpression
    @Override
    public PnvzValue visitNumberExpression(@NotNull PnvzParser.NumberExpressionContext ctx) {
        return new PnvzValue(Double.valueOf(ctx.getText()));
    }

    // Bool                                     #boolExpression
    @Override
    public PnvzValue visitBoolExpression(@NotNull PnvzParser.BoolExpressionContext ctx) {
        return new PnvzValue(Boolean.valueOf(ctx.getText()));
    }



    private PnvzValue resolveIndexes(ParserRuleContext ctx, PnvzValue val, List<ExpressionContext> indexes) {
    	for (ExpressionContext ec: indexes) {
    		PnvzValue idx = this.visit(ec);
    		if (!idx.isNumber() || (!val.isList() && !val.isString()) ) {
        		throw new EvalException("Problem resolving indexes on "+val+" at "+idx, ec);
    		}
    		int i = idx.asDouble().intValue();
    		if (val.isString()) {
    			val = new PnvzValue(val.asString().substring(i, i+1));
    		} else {
    			val = val.asList().get(i);
    		}
    	}
    	return val;
    }
    
    private void setAtIndex(ParserRuleContext ctx, List<ExpressionContext> indexes, PnvzValue val, PnvzValue newVal) {
    	if (!val.isList()) {
    		throw new EvalException(ctx);
    	}
    	// TODO some more list size checking in here
    	for (int i = 0; i < indexes.size() - 1; i++) {
    		PnvzValue idx = this.visit(indexes.get(i));
    		if (!idx.isNumber()) {
        		throw new EvalException(ctx);
    		}
    		val = val.asList().get(idx.asDouble().intValue());
    	}
    	PnvzValue idx = this.visit(indexes.get(indexes.size() - 1));
		if (!idx.isNumber()) {
    		throw new EvalException(ctx);
		}
    	val.asList().set(idx.asDouble().intValue(), newVal);
    }
    
    // functionCall                     #functionCallExpression
    @Override
    public PnvzValue visitFunctionCallExpression(FunctionCallExpressionContext ctx) {
    	PnvzValue val = this.visit(ctx.functionCall());
    	return val;
    }


    // Identifier                       #identifierExpression
    @Override
    public PnvzValue visitIdentifierExpression(@NotNull PnvzParser.IdentifierExpressionContext ctx) {
        String id = ctx.Identifier().getText();
        PnvzValue val = scope.resolve(id);

        return val;
    }

    // String                          #stringExpression
    @Override
    public PnvzValue visitStringExpression(@NotNull PnvzParser.StringExpressionContext ctx) {
        String text = ctx.getText();
        text = text.substring(1, text.length() - 1).replaceAll("\\\\(.)", "$1");
        PnvzValue val = new PnvzValue(text);
        return val;
    }

    // '(' expression ')' indexes?              #expressionExpression
    @Override
    public PnvzValue visitExpressionExpression(ExpressionExpressionContext ctx) {
        PnvzValue val = this.visit(ctx.expression());
        return val;
    }



    
    // assignment
    // : Identifier indexes? '=' expression
    // ;
    @Override
    public PnvzValue visitAssignment(@NotNull PnvzParser.AssignmentContext ctx) {
        PnvzValue newVal = this.visit(ctx.expression());

        	String id = ctx.Identifier().getText();
        	scope.assign(id, newVal);

        return PnvzValue.VOID;
    }

    // Identifier '(' exprList? ')' #identifierFunctionCall
    @Override
    public PnvzValue visitIdentifierFunctionCall(IdentifierFunctionCallContext ctx) {
        List<ExpressionContext> params = ctx.exprList() != null ? ctx.exprList().expression() : new ArrayList<ExpressionContext>();
        String id = ctx.Identifier().getText() + params.size();
        Function function;      
        if ((function = functions.get(id)) != null) {
            return function.invoke(params, functions, scope);
        }
        throw new EvalException(ctx);
    }

    // Println '(' expression? ')'  #printlnFunctionCall
    @Override
    public PnvzValue visitPrintlnFunctionCall(@NotNull PnvzParser.PrintlnFunctionCallContext ctx) {
        System.out.println(this.visit(ctx.expression()));
        return PnvzValue.VOID;
    }

    // Print '(' expression ')'     #printFunctionCall
    @Override
    public PnvzValue visitPrintFunctionCall(@NotNull PnvzParser.PrintFunctionCallContext ctx) {
        System.out.print(this.visit(ctx.expression()));
        return PnvzValue.VOID;
    }



    // ifStatement
    //  : ifStat elseIfStat* elseStat? End
    //  ;
    //
    // ifStat
    //  : If expression Do block
    //  ;
    //
    // elseIfStat
    //  : Else If expression Do block
    //  ;
    //
    // elseStat
    //  : Else Do block
    //  ;
    @Override
    public PnvzValue visitIfStatement(@NotNull PnvzParser.IfStatementContext ctx) {

        // if ...
        if(this.visit(ctx.ifStat().expression()).asBoolean()) {
            return this.visit(ctx.ifStat().block());
        }

        // else if ...
        for(int i = 0; i < ctx.elseIfStat().size(); i++) {
            if(this.visit(ctx.elseIfStat(i).expression()).asBoolean()) {
                return this.visit(ctx.elseIfStat(i).block());
            }
        }

        // else ...
        if(ctx.elseStat() != null) {
            return this.visit(ctx.elseStat().block());
        }

        return PnvzValue.VOID;
    }
    
    // block
    // : (statement | functionDecl)* (Return expression)?
    // ;
    @Override
    public PnvzValue visitBlock(BlockContext ctx) {
    		
    	scope = new Scope(scope); // create new local scope
        for (StatementContext sx: ctx.statement()) {
            this.visit(sx);
        }
        ExpressionContext ex;
        if ((ex = ctx.expression()) != null) {
        	returnValue.value = this.visit(ex);
        	scope = scope.parent();
        	throw returnValue;
        }
        scope = scope.parent();
        return PnvzValue.VOID;
    }
    
    // forStatement
    // : For Identifier '=' expression To expression OBrace block CBrace
    // ;
    @Override
    public PnvzValue visitForStatement(ForStatementContext ctx) {
        int start = this.visit(ctx.expression(0)).asDouble().intValue();
        int stop = this.visit(ctx.expression(1)).asDouble().intValue();
        for(int i = start; i <= stop; i++) {
            scope.assign(ctx.Identifier().getText(), new PnvzValue(i));
            PnvzValue returnValue = this.visit(ctx.block());
            if(returnValue != PnvzValue.VOID) {
                return returnValue;
            }
        }
        return PnvzValue.VOID;
    }
    
    // whileStatement
    // : While expression OBrace block CBrace
    // ;
    @Override
    public PnvzValue visitWhileStatement(WhileStatementContext ctx) {
        while( this.visit(ctx.expression()).asBoolean() ) {
            PnvzValue returnValue = this.visit(ctx.block());
            if (returnValue != PnvzValue.VOID) {
                return returnValue;
            }
        }
        return PnvzValue.VOID;
    }
    
}
