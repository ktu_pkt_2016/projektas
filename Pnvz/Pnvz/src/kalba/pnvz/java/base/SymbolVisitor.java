package kalba.pnvz.java.base;

import kalba.pnvz.java.generated.PnvzBaseVisitor;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;
import kalba.pnvz.java.generated.PnvzParser.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SymbolVisitor extends PnvzBaseVisitor<PnvzValue> {
    Map<String, Function> functions;
    
    public SymbolVisitor(Map<String, Function> functions) {
        this.functions = functions;
    }
    
    @Override
    public PnvzValue visitFunctionDecl(FunctionDeclContext ctx) {
        List<TerminalNode> params = ctx.idList() != null ? ctx.idList().Identifier() : new ArrayList<TerminalNode>();
        ParseTree block = ctx.block();
        String id = ctx.Identifier().getText() + params.size();
        functions.put(id, new Function(id, params, block));
        return PnvzValue.VOID;
    }
}
