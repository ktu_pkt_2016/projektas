// Generated from C:/Users/lukmil/IdeaProjects/Pnvz/src/kalba/pnvz/antlr4\Pnvz.g4 by ANTLR 4.5.1
package kalba.pnvz.java.generated;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link PnvzParser}.
 */
public interface PnvzListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link PnvzParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(PnvzParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link PnvzParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(PnvzParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link PnvzParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(PnvzParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link PnvzParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(PnvzParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link PnvzParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(PnvzParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PnvzParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(PnvzParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PnvzParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(PnvzParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link PnvzParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(PnvzParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identifierFunctionCall}
	 * labeled alternative in {@link PnvzParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterIdentifierFunctionCall(PnvzParser.IdentifierFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identifierFunctionCall}
	 * labeled alternative in {@link PnvzParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitIdentifierFunctionCall(PnvzParser.IdentifierFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code printlnFunctionCall}
	 * labeled alternative in {@link PnvzParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterPrintlnFunctionCall(PnvzParser.PrintlnFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code printlnFunctionCall}
	 * labeled alternative in {@link PnvzParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitPrintlnFunctionCall(PnvzParser.PrintlnFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code printFunctionCall}
	 * labeled alternative in {@link PnvzParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterPrintFunctionCall(PnvzParser.PrintFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code printFunctionCall}
	 * labeled alternative in {@link PnvzParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitPrintFunctionCall(PnvzParser.PrintFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assertFunctionCall}
	 * labeled alternative in {@link PnvzParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterAssertFunctionCall(PnvzParser.AssertFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assertFunctionCall}
	 * labeled alternative in {@link PnvzParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitAssertFunctionCall(PnvzParser.AssertFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code sizeFunctionCall}
	 * labeled alternative in {@link PnvzParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterSizeFunctionCall(PnvzParser.SizeFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code sizeFunctionCall}
	 * labeled alternative in {@link PnvzParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitSizeFunctionCall(PnvzParser.SizeFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link PnvzParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(PnvzParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PnvzParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(PnvzParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PnvzParser#ifStat}.
	 * @param ctx the parse tree
	 */
	void enterIfStat(PnvzParser.IfStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link PnvzParser#ifStat}.
	 * @param ctx the parse tree
	 */
	void exitIfStat(PnvzParser.IfStatContext ctx);
	/**
	 * Enter a parse tree produced by {@link PnvzParser#elseIfStat}.
	 * @param ctx the parse tree
	 */
	void enterElseIfStat(PnvzParser.ElseIfStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link PnvzParser#elseIfStat}.
	 * @param ctx the parse tree
	 */
	void exitElseIfStat(PnvzParser.ElseIfStatContext ctx);
	/**
	 * Enter a parse tree produced by {@link PnvzParser#elseStat}.
	 * @param ctx the parse tree
	 */
	void enterElseStat(PnvzParser.ElseStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link PnvzParser#elseStat}.
	 * @param ctx the parse tree
	 */
	void exitElseStat(PnvzParser.ElseStatContext ctx);
	/**
	 * Enter a parse tree produced by {@link PnvzParser#functionDecl}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDecl(PnvzParser.FunctionDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link PnvzParser#functionDecl}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDecl(PnvzParser.FunctionDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link PnvzParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void enterForStatement(PnvzParser.ForStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PnvzParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void exitForStatement(PnvzParser.ForStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PnvzParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(PnvzParser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PnvzParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(PnvzParser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PnvzParser#idList}.
	 * @param ctx the parse tree
	 */
	void enterIdList(PnvzParser.IdListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PnvzParser#idList}.
	 * @param ctx the parse tree
	 */
	void exitIdList(PnvzParser.IdListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PnvzParser#exprList}.
	 * @param ctx the parse tree
	 */
	void enterExprList(PnvzParser.ExprListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PnvzParser#exprList}.
	 * @param ctx the parse tree
	 */
	void exitExprList(PnvzParser.ExprListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ltExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLtExpression(PnvzParser.LtExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ltExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLtExpression(PnvzParser.LtExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code gtExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGtExpression(PnvzParser.GtExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code gtExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGtExpression(PnvzParser.GtExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBoolExpression(PnvzParser.BoolExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBoolExpression(PnvzParser.BoolExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notEqExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNotEqExpression(PnvzParser.NotEqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notEqExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNotEqExpression(PnvzParser.NotEqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNumberExpression(PnvzParser.NumberExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNumberExpression(PnvzParser.NumberExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIdentifierExpression(PnvzParser.IdentifierExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIdentifierExpression(PnvzParser.IdentifierExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code modulusExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterModulusExpression(PnvzParser.ModulusExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code modulusExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitModulusExpression(PnvzParser.ModulusExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiplyExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplyExpression(PnvzParser.MultiplyExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiplyExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplyExpression(PnvzParser.MultiplyExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code gtEqExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGtEqExpression(PnvzParser.GtEqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code gtEqExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGtEqExpression(PnvzParser.GtEqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code divideExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDivideExpression(PnvzParser.DivideExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code divideExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDivideExpression(PnvzParser.DivideExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code orExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterOrExpression(PnvzParser.OrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code orExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitOrExpression(PnvzParser.OrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eqExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterEqExpression(PnvzParser.EqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eqExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitEqExpression(PnvzParser.EqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAndExpression(PnvzParser.AndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAndExpression(PnvzParser.AndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code inExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterInExpression(PnvzParser.InExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code inExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitInExpression(PnvzParser.InExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterStringExpression(PnvzParser.StringExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitStringExpression(PnvzParser.StringExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expressionExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpressionExpression(PnvzParser.ExpressionExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expressionExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpressionExpression(PnvzParser.ExpressionExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code addExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAddExpression(PnvzParser.AddExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code addExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAddExpression(PnvzParser.AddExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subtractExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSubtractExpression(PnvzParser.SubtractExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subtractExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSubtractExpression(PnvzParser.SubtractExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNullExpression(PnvzParser.NullExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNullExpression(PnvzParser.NullExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionCallExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCallExpression(PnvzParser.FunctionCallExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionCallExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCallExpression(PnvzParser.FunctionCallExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ltEqExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLtEqExpression(PnvzParser.LtEqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ltEqExpression}
	 * labeled alternative in {@link PnvzParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLtEqExpression(PnvzParser.LtEqExpressionContext ctx);
}